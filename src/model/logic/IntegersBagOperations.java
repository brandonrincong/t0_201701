package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	public int getMin(IntegersBag bag){
		int max = Integer.MAX_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max > value){
					max = value;
				}
			}
			
		}
		return max;
	}
	public int getPromedio(IntegersBag bag){
		int promedio=Integer.MIN_VALUE;
		int coutn=0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				promedio+=(int)iter.next();
				coutn++;
				}
			promedio=promedio/coutn;
		}
		return promedio;
	}
	public  boolean isEmpy(IntegersBag bag){
		return bag==null;
	}
}
